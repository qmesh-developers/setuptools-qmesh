#    Copyright (C) 2013 Alexandros Avdis and others.
#    See the AUTHORS.md file for a full list of copyright holders.
#
#    This file is part of setuptools-qmesh.
#
#    setuptools-qmesh is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    setuptools-qmesh is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with setuptools-qmesh.  If not, see <http://www.gnu.org/licenses/>.
'''Module containing tests of setuptools-qmesh command line options.

This module tests that all command line options introduced by setuptools-qmesh
complete, with a zero-code returned.
'''

import os
import tempfile
import subprocess
from .. import setuptools_qmesh_test

class TestCommandLineExtensions(setuptools_qmesh_test.SetuptoolsQmeshTest):
    '''Test class for setuptools-qmesh command line extensions.

    Test that all command line options introduced by setuptools-qmesh complete.
    The methods run individual commands with options and check that an appropriate
    exit code is returned.
    '''

    def test_help_commands(self):
        '''Method testing the setup.py script with the --help-commands option.
        '''
        #Construct path to the setup.py script
        setup_script_path = os.path.join(self.project_root_path, 'setup.py')
        #Assemble string of command to test
        command = ['python' + self.python_version, setup_script_path, '--help-commands']
        #Invoke command on setup.py script
        self.invoke_and_test_command(command)

    def invoke_and_test_command(self, command, expected_return_code=0):
        '''Method invoking setuptools commands and checking return codes.

        The setuptools commands are issued using the Python version that runs the test.
        '''
        #Invoke command and capture stdout & stderr to temporary files
        command_stdout = tempfile.TemporaryFile(mode='w+b')
        command_stderr = tempfile.TemporaryFile(mode='w+b')
        command_proc = subprocess.Popen(command,
                                        stdout=command_stdout,
                                        stderr=command_stderr)
        command_proc.wait()
        #If command had issues, echo its stdout and stdout
        if command_proc.returncode != expected_return_code:
            command_stdout.seek(0)
            self.log(40, '***command stdout:')
            for line in command_stdout:
                self.log(40, line.decode('utf-8').strip('\n'))
            command_stderr.seek(0)
            self.log(40, '***command stderr:')
            for line in command_stderr:
                self.log(40, line.decode('utf-8').strip('\n'))
        #Close files
        command_stdout.close()
        command_stderr.close()
        #Make sure command has run without issues
        self.assertEqual(command_proc.returncode,
                         expected_return_code,
                         'The command '+ ' '.join(command) +\
                         ' returned a non-zero code: '+str(command_proc.returncode)+'.'+\
                         ' Please check command output.')
