setuptools-qmesh
=================

setuptools-qmesh provides setuptools extensions that facilitate installation of qmesh packages.



Installation
-----------------

You do not have to install setuptools-qmesh yourself. setuptools-qmesh will be automatically downloaded from PyPI when required, during installation of other qmesh packages.

See the `qmesh synoptic manual <https://qmesh-synoptic-manual.readthedocs.io/en/latest>`_ for more information.



Development, Maintenance and Licence
----------------------------------------

setuptools-qmesh is developed and maintained by `Alexandros Avdis <https://orcid.org/0000-0002-2695-3358>`_ and `Jon Hill <https://orcid.org/0000-0003-1340-4373>`_.
Please see file `AUTHORS.md <https://bitbucket.org/qmesh-developers/setuptools-qmesh/raw/HEAD/AUTHORS.md>`_ for more information.

setuptools-qmesh is distributed under GPL v3.
The file `LICENCE <https://bitbucket.org/qmesh-developers/setuptools-qmesh/raw/HEAD/LICENSE>`_ contains a complete copy of the licence.

The package setuptools-qmesh is open-source.
The source code is freely available to download and adapt, subject to the conditions stated in the `LICENCE <https://bitbucket.org/qmesh-developers/setuptools-qmesh/raw/HEAD/LICENSE>`_ file.
However, we adopted a closed-development approach, where maintenance and development is carried out by the package originators.
We have opted for this approach to limit the resources needed for development. A larger development team necessitates larger management structures and significant CI/CD expenditure.
Larger management structures will absorb time for other exciting and useful research, while CI/CD expenditure could threaten our aim to offer a focused package, at no monetary costs to the users.



Documentation 
-----------------

You can access relevant documentation through the following avenues:

* The `qmesh website <https://www.qmesh.org>`_.
* The `qmesh synoptic manual <https://qmesh-synoptic-manual.readthedocs.io/en/latest>`_.
* The `setuptools-qmesh reference manual <https://setuptools-qmesh.readthedocs.io/en/latest>`_.
