.. This file is part of setuptools-qmesh manual
   Copyright (C) 2017 Alexandros Avdis and Jon Hill
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

*********************************
API Reference
*********************************

Installation script
=====================

.. automodule:: setup
   :members:

Package modules
=====================
.. automodule:: setuptools_qmesh
   :members:

dist module
------------

.. automodule:: setuptools_qmesh.dist
   :members:

command module
--------------

.. automodule:: setuptools_qmesh.command
   :members:


check_gmsh module
^^^^^^^^^^^^^^^^^
.. automodule:: setuptools_qmesh.command.check_gmsh
   :members:

check_qgis module
^^^^^^^^^^^^^^^^^
.. automodule:: setuptools_qmesh.command.check_qgis
   :members:

egg_info module
^^^^^^^^^^^^^^^^^
.. automodule:: setuptools_qmesh.command.egg_info
   :members:
