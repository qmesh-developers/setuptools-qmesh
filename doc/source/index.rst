.. This file is part of setuptools-qmesh manual
   Copyright (C) 2017 Alexandros Avdis.
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

Contents
============================================

| Copyright (C) 2017 Alexandros Avdis & Jon Hill
|
| Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.3  or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. A copy of the license is included in the section entitled "GNU  Free Documentation License".
|

Welcome to the setuptools-qmesh manual.

setuptools-qmesh is a project aimed at proving all the necessary functionality for installation of other qmesh packages. qmesh is an unstructured triangular mesh generator for geophysical domains. It is suited to mesh generation over topologically two-dimensional domains.

qmesh was developed by `Alexandros Avdis <https://orcid.org/0000-0002-2695-3358>`_ and `Jon Hill <https://orcid.org/0000-0003-1340-4373>`_, primarily to meet our meshing needs. We developed qmesh with an open-source philosophy in mind, so you are welcome to view the qmesh code and use qmesh, under the `GNU General Public License, version 3 <https://www.gnu.org/licenses/gpl-3.0.en.html>`_. To facilitate installation, the setuptools-qmesh project was created as a separate repository. All the necessary functionality to check and install dependencies as well as test qmesh packages is maintained in setuptools-qmesh.

A typical qmesh user should not have to consult the present manual. The interaction between qmesh packages and setuptools-qmesh should be transparent to the user. However, we have created this manual for:
 #. Completeness,
 #. Our own reference when maintaining the code,
 #. Reference of others; programmers looking to create a similar partitioning (installation versus core package) in their own work.

Contents:

.. toctree::
   :maxdepth: 2
   :numbered:

   introduction
   architecture
   developmentDistributionLicence
   api_reference
   tests_reference

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

