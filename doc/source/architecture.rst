.. This file is part of setuptools-qmesh manual
   Copyright (C) 2017 Alexandros Avdis and Jon Hill
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

Architecture
============================================

.. Broad description.

.. Description of modules and the functionality each module offers, with reference to setuptools.

.. Description of execution sequence: Which method is expected to call what.
