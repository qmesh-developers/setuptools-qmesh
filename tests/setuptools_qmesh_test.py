#    Copyright (C) 2022 Alexandros Avdis and others.
#    See the AUTHORS.md file for a full list of copyright holders.
#
#    This file is part of setuptools-qmesh.
#
#    setuptools-qmesh is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    setuptools-qmesh is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with setuptools-qmesh.  If not, see <http://www.gnu.org/licenses/>.
'''Module containing base class of setuptools qmesh tests
'''

import unittest
import os
import sys
import logging

class SetuptoolsQmeshTest(unittest.TestCase):
    '''Class used to derive all qmesh tests.

    This class is the base from which all qmesh test classes are derived.
    This class is itself derived from the unittest.TestCase class, and
    the methods setUp and tearDown are re-defined.
    '''

    def setUp(self):
        '''Method setting-up test environment.

        This method is not intended to be invoked by the user. It stores:
        1. The path of the testing class. Note, this path will be the path
           of this file, so derived classes might have to redefine it.
        2. The path of the tests directory.
        3. The project root path.
        4. The python version used to invoke the test.
        5. An empty list of output files, populated by derived testing
           classes, when files are produced.
        '''
        # Create logger object
        self.log = logging.log
        # Store absolute path to the test file, without the test file-name
        self.this_path = os.path.dirname(os.path.realpath(__file__))
        # Construct absolute path to the "tests" directory
        path = self.this_path
        while os.path.split(path)[1] != "tests":
            path = os.path.split(path)[0]
        self.project_test_path = path
        # Store absolute path to the project root
        self.project_root_path = os.path.split(self.project_test_path)[0]
        # Detect python version used to run test, as a string in the
        # form 'major.minor'
        python_version_info = sys.version_info
        self.python_version = str(python_version_info.major) + \
                              '.' + str(python_version_info.minor)
        # Define list of output files, empty for now
        self.output_files = []


    def remove_file(self, filename):
        '''Remove files, without failing for files that do not exist.

        This method uses the method remove from module os to remove files, but
        captures the exception thrown in case the files do not exist.
        Exceptions due to other reasons are still raised.
        '''
        try:
            os.remove(filename)
        except FileNotFoundError:
            pass
        self.log(logging.INFO, 'Removed file '+filename)


    def tearDown(self):
        '''Method taking down the test fixture, tiding up.

        Test-cases typically generate files that can obscure working. This method
        will remove all files identified in the setUp method as output files.
        The output files are ignored by the git set-up.
        '''
        for filename in self.output_files:
            self.remove_file(filename)
