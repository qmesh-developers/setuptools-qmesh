.. This file is part of setuptools-qmesh manual
   Copyright (C) 2017 Alexandros Avdis and Jon Hill
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

Introduction
============================================

.. What are qmesh and setuptools-qmesh?
qmesh is an unstructured triangular mesh generator for geophysical domains.
It is suited to mesh generation over topologically two-dimensional domains typically encountered in medium-to-large scale geophysical modelling, as discussed in :cite:`Avdis_et_al:2018`.
qmesh can be thought as a suite of packages, each offering a different user interface: a Python API, a Command Line Interface and a Graphical User Interface (for detailed information about all the packages and their purpose, the reader is referred to the qmesh synoptic manual).
Given the complexities of building distributing, installing and testing, the present package, setuptools-qmesh, was created to encapsulate and facilitate installation and testing of other qmesh packages. 

.. Overview of pip, setuptools, distutils and how setuptools-qmesh extends setuptools
Software is typically build to achieve a purpose, expressed by a specification.
Such objectives are rarely robustly achieved without depending on other software.
A good design will utilise software libraries, or other forms of code, to carry out specialised operations.
For example qmesh uses qgis (as a specialised Geographical Information System) and gmsh (as a specialised mesh generator) to achieve its purpose:cite:`Avdis_et_al:2018`.
Such code is typically termed *a dependency*.
However, dependencies can be hard to maintain. For example, a package might require a specific version of a dependency, while another package might require the same dependency at a different version.
It is the purpose of a package manager to manage the dependencies.
Things often go wrong, but in this case a good package manager offers utilities to help diagnose and correct the situation.
``pip`` is a very popular package manager for Python packages, and it includes distribution and installation utilities.
One of the reasons for its popularity is the availability of an online repository, the `Python Package Index (PyPI) <https://pypi.python.org/pypi>`_, where many popular Python packages are hosted.
``pip`` can be used to download and install packages and dependencies from PyPI with great ease, if those dependencies exist in PyPI themselves.
On a Linux command terminal (everything discussed in this manual assumes a Unix/Linux environment), the user only has to issue ``pip install qmesh`` to download and install the qmesh python API.
Therefore, given the ease Python packages can be installed and distributed through ``pip`` and PyPI, they have been chosen as the preferred methods of qmesh distribution and installation.

.. Why is setuptools-qmesh developed in a repository separate to other qmesh repos?

.. How setuptools-qmesh is used/invoked in other qmesh packages.

.. What functionality does setuptools-qmesh introduce
``pip`` uses `Python setuptools <https://pypi.org/project/setuptools/>`_ to install packages.
Following the ``setuptools`` architecture, a python script, named ``setup.py``, is created by the package maintainers where a call to the ``setuptools.setup`` function facilitates various operations: installation, testing or building documentation. 
``setuptools`` also allows custom operations to be defined.
Therefore, package developers have the flexibility of defining non-standard operations, depending on their package needs.
In qmesh all such custom operations are contained by the ``setuptools-qmesh`` package and are:

*  Checking for a functional gmsh installation.
*  Checking for a functional qgis installation.
*  Storing the details of the qmesh authors for attribution and support purposes.
*  Storing the licence of qmesh, for distribution and support purposes.
*  Storing the GIT SHA key from the qmesh repository used to package qmesh, for distribution and support purposes.
*  Storing the path to the gmsh installation, so that gmsh can be invoked by qmesh.
*  Storing the path to the qgis installation, so that qgis can be used by qmesh.

You can list all commands in the ``setuptools-qmesh`` package by invoking its ``setup.py`` script, with the ``--help-commands`` option:

.. code-block:: bash

    $ python setup.py --help-commands
    Standard commands:
      build             build everything needed to install
      build_py          "build" pure Python modules (copy to build directory)
      build_ext         build C/C++ extensions (compile/link to build directory)
      build_clib        build C/C++ libraries used by Python extensions
      build_scripts     "build" scripts (copy and fixup #! line)
      clean             clean up temporary files from 'build' command
      install           install everything from build directory
      install_lib       install all Python modules (extensions and pure Python)
      install_headers   install C/C++ header files
      install_scripts   install scripts (Python or otherwise)
      install_data      install data files
      sdist             create a source distribution (tarball, zip file, etc.)
      register          register the distribution with the Python package index
      bdist             create a built (binary) distribution
      bdist_dumb        create a "dumb" built distribution
      bdist_rpm         create an RPM distribution
      bdist_wininst     create an executable installer for MS Windows
      upload            upload binary package to PyPI
      check             perform some checks on the package
    
    Extra commands:
      saveopts          save supplied options to setup.cfg or other config file
      check_gmsh        Check gmsh is installed, by trying to find out installed version.
      develop           install package in 'development mode'
      install_non_pip   Check non-PyPI packages are installed on the host system, and attempt to install if not.
      check_qgis        Check qgis is installed.
      isort             Run isort on modules registered in setuptools
      test              run unit tests after in-place build
      setopt            set an option in setup.cfg or another config file
      install_egg_info  Install an .egg-info directory for the package
      rotate            delete older distributions, keeping N newest files
      egg_info          create a distribution's .egg-info directory
      upload_docs       Upload documentation to PyPI
      alias             define a shortcut to invoke one or more commands
      easy_install      Find/get/install Python packages
      bdist_egg         create an "egg" distribution
      dist_info         create a .dist-info directory

    usage: setup.py [global_opts] cmd1 [cmd1_opts] [cmd2 [cmd2_opts] ...]
       or: setup.py --help [cmd1 cmd2 ...]
       or: setup.py --help-commands
       or: setup.py cmd --help

Under the *Extra commands* section above, the commands ``check_gmsh`` and ``check_qgis`` were introduced by the ``setuptools-qmesh`` package.
The rest of the commands are introduced by setuptools and are available in all packages using setuptools.
The ``usage`` section above indicates that each command has *options* associated to it, and differentiates the *command options* from the *global options*.
Evidently, ``--help-commands`` is a global option.
You can list the options of the commands ``check_gmsh``, ``check_qgis`` and ``install_non_pip`` as follows:

.. code-block:: bash

    $ python setup.py --help install_non_pip check_gmsh check_qgis
    Common commands: (see '--help-commands' for more)

      setup.py build      will build the package underneath 'build/'
      setup.py install    will install the package

    Global options:
      --verbose (-v)  run verbosely (default)
      --quiet (-q)    run quietly (turns verbosity off)
      --dry-run (-n)  don't actually do anything
      --help (-h)     show detailed help message
      --no-user-cfg   ignore pydistutils.cfg in your home directory

    Options for 'InstallNonPip' command:
      --install-pyrdm  Boolean for installing PyRDM (True/False), defaults to
                       True.
      --pyrdm-path     Path to search for, or install PyRDM.

    Options for 'CheckGmsh' command:
      --gmsh-bin-path  Path to gmsh binary (executable)

    Options for 'CheckQgis' command:
      --qgis-path  Path to qgis installation

    usage: setup.py [global_opts] cmd1 [cmd1_opts] [cmd2 [cmd2_opts] ...]
       or: setup.py --help [cmd1 cmd2 ...]
       or: setup.py --help-commands
       or: setup.py cmd --help
